Rails.application.routes.draw do
  resources :bookmarks, only: [:index, :new, :create, :edit, :update]

  root to: 'bookmarks#index'
end
