class AddTagsToBookmarks < ActiveRecord::Migration[5.0]
  def change
    add_column :bookmarks, :tags, :text, array: true, default: []
    add_index  :bookmarks, :tags, using: 'gin'
  end
end
