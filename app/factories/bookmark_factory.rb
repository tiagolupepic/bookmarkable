class BookmarkFactory
  attr_reader   :params, :site, :tags
  attr_accessor :bookmark

  def initialize(params)
    @params = params
  end

  def create
    find_or_create_site
    build_tags
    create_bookmark
  end

  def update(bookmark)
    @bookmark = bookmark

    find_or_create_site
    build_tags
    bookmark.site = site if site
    bookmark.update_attributes params.merge(tags: tags)
  end

  private

  def find_or_create_site
    return if host_name.blank?

    @site ||= Site.find_or_create_by url: url
  end

  def create_bookmark
    @bookmark = Bookmark.new(params.merge(site: site, tags: tags))
    bookmark.save
  end

  def build_tags
    @tags ||= params[:tags].to_s.split(',')
  end

  def host_name
    return if params[:url].blank?

    URI.parse(params[:url]).host
  end

  def scheme_name
    return if params[:url].blank?

    URI.parse(params[:url]).scheme
  end

  def url
    scheme_name[0..3] + '://' + host_name
  end
end
