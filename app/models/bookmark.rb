class Bookmark < ApplicationRecord
  include PgSearch

  belongs_to :site

  validates :title, :url, :site, presence: true
  validates :url, format: VALID_URL_FORMAT

  pg_search_scope :search, against: [:title, :url, :shortening, :tags],
                           using: {
                            tsearch: { prefix: true }
                          }
end
