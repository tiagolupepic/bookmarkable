class Site < ApplicationRecord
  validates :url, presence: true
  validates :url, format: VALID_URL_FORMAT
end
