module BookmarkHelper
  def tags_inline(bookmark)
    bookmark.tags.join(', ')
  end
end
