class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception

  rescue_from ActiveRecord::RecordNotFound, with: :render_not_found_response

  def render_not_found_response
    render file: "#{Rails.root}/public/404.html", layout: false, :status => 404
  end
end
