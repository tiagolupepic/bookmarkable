class BookmarksController < ApplicationController
  def index
    @bookmarks = Bookmark.order(created_at: :desc)
    @bookmarks = @bookmarks.search(params[:q]) if params[:q].present?
  end

  def new
    @bookmark = Bookmark.new
  end

  def create
    if factory.create
      redirect_to bookmarks_url, notice: 'Bookmark was successfully created.'
    else
      @bookmark = factory.bookmark
      render :new
    end
  end

  def edit
    bookmark
  end

  def update
    if factory.update(bookmark)
      redirect_to bookmarks_url, notice: 'Bookmark was successfully updated.'
    else
      render :edit
    end
  end

  private

  def factory
    @factory ||= BookmarkFactory.new(bookmark_params)
  end

  def bookmark
    @bookmark ||= Bookmark.find(params[:id])
  end

  def bookmark_params
    params.require(:bookmark).permit(:title, :url, :shortening, :tags)
  end
end
