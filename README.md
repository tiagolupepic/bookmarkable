# Bookmarkable

> Simple application using Rails 5 to store bookmarks with Postgres.

## Features

- Store bookmarks with title, url, description and tags
- Search bookmarks by any field including tags


## Requirements

- Ruby 2.3.3
- Postgresql 9.4+
- Docker Compose

## Installation

Download Bookmarkable source code:

  `git clone git@bitbucket.org:tiagolupepic/bookmarkable.git`

Enter the directory

  `cd bookmarkable`

Then execute:

  `docker-compose build bookmarkable`

Create development database:

  `docker-compose run --rm bookmarkable bundle exec rake db:create`

Then run migrations:

  `docker-compose run --rm bookmarkable bundle exec rake db:migrate`

Run it!

  `docker-compose up bookmarkable`

App url address is: `http://localhost:3000`

## Run Tests

First, create a test database:

  `docker-compose run --rm bookmarkable bundle exec rake db:create RAILS_ENV=test`

Then run migrations:

  `docker-compose run --rm bookmarkable bundle exec rake db:migrate RAILS_ENV=test`

Run tests

  `docker-compose run --rm bookmarkable bin/rspec`
