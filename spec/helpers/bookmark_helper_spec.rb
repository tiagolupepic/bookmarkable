require 'rails_helper'

RSpec.describe BookmarkHelper, type: :helper do
  describe '#tags_inline' do
    let(:bookmark) { create(:bookmark, :with_tags) }

    it 'should join tags' do
      expect(helper.tags_inline(bookmark)).to eq 'github, code, repo, lupepic'
    end
  end
end
