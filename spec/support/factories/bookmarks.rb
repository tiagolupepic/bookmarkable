FactoryGirl.define do
  factory :bookmark do
    title      'Tiago Lupepic Github'
    url        'https://github.com/tiagolupepic'
    shortening 'Code repositories of Tiago Lupepic'
    site       { build(:site) }

    trait :with_tags do
      tags ['github', 'code', 'repo', 'lupepic']
    end
  end
end
