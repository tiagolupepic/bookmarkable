require 'rails_helper'

RSpec.describe BookmarkFactory do
  let(:params) { {} }

  subject { described_class.new(params) }

  describe '#create' do
    let(:params) { { title: 'Github', url: 'https://github.com/', shortening: 'Built for developers' } }

    it 'should create bookmark' do
      subject.create

      expect(Bookmark.count).to eq 1
    end

    it 'should create site' do
      subject.create

      expect(Site.count).to eq 1
    end

    it 'should assign url to site' do
      subject.create

      site = Site.last
      expect(site.url).to eq 'http://github.com'
    end

    context 'when already exist site' do
      before { create(:site) }

      it 'should not duplicate site' do
        subject.create

        expect(Site.count).to eq 1
      end
    end

    context 'when url dont have https' do
      let(:params) { { title: 'Github', url: 'http://github.com', shortening: 'Built for developers' } }

      it 'should assign url to site' do
        subject.create

        site = Site.last
        expect(site.url).to eq 'http://github.com'
      end
    end

    context 'when dont have required params' do
      let(:params) { { title: nil, url: 'http://github.com', shortening: 'Built for developers' } }

      it 'should not create bookmark' do
        subject.create

        expect(Bookmark.count).to eq 0
      end

      it 'should create site' do
        subject.create

        expect(Site.count).to eq 1
      end

      it 'should be false' do
        expect(subject.create).to be_falsy
      end

      it 'should assign bookmark to accessor' do
        subject.create

        expect(subject.bookmark).to_not be_persisted
      end
    end

    context 'when params dont have tags' do
      let(:params) { { title: 'Github', url: 'http://github.com', shortening: 'Built for developers', tags: nil } }

      it 'should skip to build' do
        subject.create

        bookmark = Bookmark.last

        expect(bookmark.tags.count).to eq 0
      end
    end

    context 'when params have tags' do
      let(:params) { { title: 'Github', url: 'http://github.com', shortening: 'Built for developers', tags: 'hello, world, repo, github' } }

      it 'should build and add' do
        subject.create

        bookmark = Bookmark.last

        expect(bookmark.tags.count).to eq 4
      end
    end
  end

  describe '#update' do
    let(:bookmark) { create(:bookmark) }
    let(:params)   { { title: 'Tiago Lupepic', url: 'https://github.com/tiagolupepic', shortening: 'Changed shortening text' } }

    subject { described_class.new(params) }

    it 'should update bookmark' do
      subject.update(bookmark)

      bookmark.reload

      expect(bookmark.title).to      eq 'Tiago Lupepic'
      expect(bookmark.shortening).to eq 'Changed shortening text'
      expect(bookmark.url).to        eq 'https://github.com/tiagolupepic'
    end

    it 'should not create another site' do
      subject.update(bookmark)

      expect(Site.count).to eq 1
    end

    context 'when change host of url' do
      let(:params) { { title: 'Tiago Lupepic', url: 'https://bitbucket.com/tiagolupepic', shortening: 'Changed shortening text' } }

      it 'should update bookmark' do
        subject.update(bookmark)

        bookmark.reload

        expect(bookmark.title).to      eq 'Tiago Lupepic'
        expect(bookmark.shortening).to eq 'Changed shortening text'
        expect(bookmark.url).to        eq 'https://bitbucket.com/tiagolupepic'
      end

      it 'should create another site' do
        subject.update(bookmark)

        expect(Site.count).to eq 2
      end

      it 'should create bitbucket record' do
        subject.update(bookmark)

        expect(Site.pluck(:url)).to eq ['http://github.com', 'http://bitbucket.com']
      end
    end

    context 'when change title' do
      let(:params) { { title: 'Changed Title' } }

      it 'should not change site' do
        subject.update(bookmark)

        expect(bookmark.site).to be_present
      end

      it 'should be true' do
        expect(subject.update(bookmark)).to be_truthy
      end
    end

    context 'when remove tags' do
      let(:params) { { tags: nil } }

      it 'should skip to build' do
        subject.update(bookmark)

        bookmark.reload

        expect(bookmark.tags.count).to eq 0
      end
    end

    context 'when add tags' do
      let(:params) { { tags: 'hello, world, repo, github' } }

      it 'should build and add' do
        subject.update(bookmark)

        bookmark.reload

        expect(bookmark.tags.count).to eq 4
      end
    end
  end
end
