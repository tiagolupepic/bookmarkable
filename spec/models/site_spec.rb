require 'rails_helper'

RSpec.describe Site, type: :model do
  it { should validate_presence_of :url }

  describe 'url format validation' do
    let(:url) { 'http://github.com' }

    subject { described_class.new(url: url) }

    it 'should be valid' do
      expect(subject).to be_valid
    end

    context 'when url is invalid' do
      let(:url) { 'github.com' }

      it 'should not be valid' do
        expect(subject).to_not be_valid
      end
    end
  end
end
