require 'rails_helper'

RSpec.describe Bookmark, type: :model do
  it { expect(described_class.ancestors).to include(PgSearch) }

  it { should validate_presence_of :title }
  it { should validate_presence_of :url }
  it { should validate_presence_of :site }

  describe 'url format validation' do
    let(:url) { 'http://github.com' }

    subject { described_class.new(attributes_for(:bookmark).merge(url: url)) }

    it 'should be valid' do
      expect(subject).to be_valid
    end

    context 'when url is invalid' do
      let(:url) { 'github.com' }

      it 'should not be valid' do
        expect(subject).to_not be_valid
      end
    end
  end
end
