require 'rails_helper'

RSpec.describe BookmarksController, type: :controller do
  describe 'GET #index' do
    before { create :bookmark }

    it 'should respond to ok' do
      get :index
      expect(response).to have_http_status(:ok)
    end

    it 'should render template' do
      get :index
      expect(response).to render_template(:index)
    end

    it 'should load bookmarks' do
      get :index
      expect(assigns(:bookmarks).count).to eq 1
    end

    context 'when have search query' do
      let(:site) { create :site }

      before do
        create :bookmark, title: 'Engine',  site: site
        create :bookmark, title: 'Search',  site: site, shortening: 'Aloha'
        create :bookmark, title: 'Another', site: site, tags: ['batman']
      end

      it 'should return all records' do
        get :index
        expect(assigns(:bookmarks).count).to eq 4
      end

      it 'should return one record' do
        get :index, params: { q: 'engine' }
        expect(assigns(:bookmarks).count).to eq 1
      end

      it 'should return one record with tags' do
        get :index, params: { q: 'batman' }
        expect(assigns(:bookmarks).count).to eq 1
      end

      it 'should return one record with shortening' do
        get :index, params: { q: 'aloha' }
        expect(assigns(:bookmarks).count).to eq 1
      end
    end
  end

  describe 'GET #new' do
    it 'should respond to ok' do
      get :new
      expect(response).to have_http_status(:ok)
    end

    it 'should render template' do
      get :new
      expect(response).to render_template(:new)
    end

    it 'should instance new Bookmark' do
      get :new
      expect(assigns(:bookmark)).to be_instance_of(Bookmark)
    end
  end

  describe 'POST #create' do
    let(:params) { attributes_for(:bookmark).except(:site) }

    it 'should respond to redirect' do
      post :create, params: { bookmark: params }
      expect(response).to have_http_status(:found)
    end

    it 'should redirect to index' do
      post :create, params: { bookmark: params }
      expect(response).to redirect_to(bookmarks_url)
    end

    context 'when fails to create' do
      let(:params) { attributes_for(:bookmark).except(:site, :title) }

      it 'should not redirect to index page' do
        post :create, params: { bookmark: params }
        expect(response).to_not redirect_to(bookmarks_url)
      end

      it 'should render new template' do
        post :create, params: { bookmark: params }
        expect(response).to render_template(:new)
      end
    end

    context 'when verify permitted params' do
      let(:params) { { title: 'Hello', url: 'https://github.com', shortening: 'Hello', tags: ['code'], fake: 'hello' } }

      it do
        should permit(:title, :url, :shortening, :tags).
          for(:create, params: { bookmark: params }).
          on(:bookmark)
      end
    end
  end

  describe 'GET #edit' do
    let(:bookmark) { create :bookmark }

    it 'should respond to ok' do
      get :edit, params: { id: bookmark.id }
      expect(response).to have_http_status(:ok)
    end

    it 'should render template' do
      get :edit, params: { id: bookmark.id }
      expect(response).to render_template(:edit)
    end

    it 'should load bookmark' do
      get :edit, params: { id: bookmark.id }
      expect(assigns(:bookmark)).to eq bookmark
    end

    context 'when bookmark not exist' do
      it 'should respond to not found' do
        get :edit, params: { id: 'not found' }
        expect(response).to have_http_status(:not_found)
      end
    end
  end

  describe 'PUT #update' do
    let(:bookmark) { create :bookmark }
    let(:params)   { { title: 'Changed Title' } }

    it 'should respond to redirect' do
      post :update, params: { id: bookmark.id, bookmark: params }
      expect(response).to have_http_status(:found)
    end

    it 'should redirect to index page' do
      post :update, params: { id: bookmark.id, bookmark: params }
      expect(response).to redirect_to(bookmarks_url)
    end

    context 'when verify permitted params' do
      let(:params) { { title: 'Hello', url: 'https://github.com', shortening: 'Hello', tags: ['code'], fake: 'hello' } }

      it do
        should permit(:title, :url, :shortening, :tags).
          for(:create, params: { bookmark: params }).
          on(:bookmark)
      end
    end

    context 'when fails to update' do
      let(:params) { { title: nil } }

      it 'should not redirect to index page' do
        post :update, params: { id: bookmark.id, bookmark: params }
        expect(response).to_not redirect_to(bookmarks_url)
      end

      it 'should render edit template' do
        post :update, params: { id: bookmark.id, bookmark: params }
        expect(response).to render_template(:edit)
      end
    end
  end
end
